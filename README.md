# gitconfig


## What is this?

This is the configuration I use for Git. It's one of a series of repositories that contain my configuration for a number of tools that I use, the installation of which is coordinated by a Chef cookbook called [homecooked](https://gitlab.com/abre/homecooked).

## Can I use it?

Sure!

To the extent possible under law, Adam Brenecki has waived all copyright and related or neighbouring rights to the content of this repository, as set out in [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/).

## What's interesting in here?

I've used [includes](https://git-scm.com/docs/git-config#_includes) for both platform-specific settings (for instance, credential helpers and mergetools), and settings I don't want version controlled (for instance, I want `core.email` to be set to my work email on my work computer, but not at home).

So, my actual `~/.gitconfig` file looks like this:

```ini
[include]
    path = ~/config/git/config
    path = ~/config/git/config.osx
    path = ~/.gitconfig_local
```

I have a bunch of aliases to write to these files, and the `~/.gitconfig` itself is set to read only so that I don't put stuff in there by accident.
